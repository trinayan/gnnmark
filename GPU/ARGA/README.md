This benchmark runs the ARGA GNN used for node classification on the Platenoid datasets Cora, CiteSeer and PubMed
It uses the PyTorch Geometric framework
ARGA currently trains on the whole graph. Mini batch training is not supported at the moment.

By default the benchmark will try to use the GPU. If a GPU is not found, it will fall back to the CPU

Sample command line : python3 argva_node_clustering.py --dataset PubMed

Note that the code will download the supported datasets automatically and place it in ../data/

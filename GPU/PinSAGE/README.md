This is the PinSAGE implementation by DGL developers.

To install the additional requirements : pip3 -r requirements.txt

To download the datasets :

chmod u+x download_data.sh
./download_data.sh


Summary  of files

1. 




To get a list of command line options:

python3 model.py --help


Sample command line execution:

For Movielens

python3 model.py  --dataset_path movielens.pkl --num-epochs 300 --num-workers 2 --device cuda:0 --hidden-dims 64

For Nowplaying

python3 model.py  --dataset_path nowplaying.pkl --num-epochs 300 --num-workers 2 --device cuda:0 --hidden-dims 64

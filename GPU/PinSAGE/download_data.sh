wget http://files.grouplens.org/datasets/movielens/ml-1m.zip
unzip ml-1m.zip
python3 process_movielens1m.py ./ml-1m ./movielens.pkl
wget  https://zenodo.org/record/3248543/files/nowplayingrs.zip?download=1
python3 process_nowplaying_rs.py ./nowplaying_rs_dataset ./nowplaying.pkl

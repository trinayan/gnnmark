This is the graphwriter benchmark

To download the data:
1. chmoud u+x download_data.sh
2. ./download_data.sh

This script will download the AGENDA dataset

To get a list of command line options:

python3 train.py --help

To run the program

python3 train.py
